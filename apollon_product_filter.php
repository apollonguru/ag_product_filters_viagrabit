<?php
/*
 * Plugin Name: AG Products Filters
 * Version: 1.0
 * Description: Apollon Guru Products Filters.
 * Author: ApollonGuru
 * Author URI: https://apollon.guru/
 *
 * @package WordPress
 * @author Hugh Lashbrooke
 * @since 1.0.0
 */
class AGProductFilter
{
	public $args,
	    $parent_product_cat_id,
	    $product_cats_print_filter,
	    $filters_lists;
	
	function __construct(Array $args)
	{
		// $this->args=$args;
		
		$this->parent_product_cat_id = (int) $args['parent_product_cat_id'];
		$this->product_cats_print_filter = (array) $args['product_cats_print_filter'];
		$this->filters_lists = (array) $args['filters_lists'];
	}
	
	public function is_print_filter()
	{
		if(!$this->parent_product_cat_id)
			return true;
        
        if(is_tax( 'product_cat', $this->product_cats_print_filter ))
            return true;
        
        return false;
	}
}

/**
 * Adds AGProductFilter_widget widget.
 */
class AGProductFilter_widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	static $filters = null;
	function __construct() 
	{
		parent::__construct(
			'agproductfilter_widget', // Base ID
			esc_html__( 'AG Products Filters', 'ag_text_domain' ), // Name
			array( 'description' => esc_html__( 'Products Filters', 'ag_text_domain' ), ) // Args
		);

		$this->filters_objects = array();

		$this->filters_objects['viagra'] = new AGProductFilter(
			array(
				'filters_lists' => array(
					array(
						'title' => 'Dosage',
						'items' => array(
							array('type' => 'product_cat', 'id' => 367),
							array('type' => 'product_cat', 'id' => 366),
							array('type' => 'product_cat', 'id' => 44),
							array('type' => 'product_cat', 'id' => 138),
							array('type' => 'product_cat', 'id' => 139),
						),
					),
					array(
						'title' => 'Sex',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2232),
							array('type' => 'product_cat', 'id' => 372),
						),
					),
					array(
						'title' => 'Amount of Tablets',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2233, 'name' => '500 Pills'),
							array('type' => 'product_cat', 'id' => 2234, 'name' => '300 Pills'),
							array('type' => 'product_cat', 'id' => 2235, 'name' => '200 Pills'),
							array('type' => 'product_cat', 'id' => 2236, 'name' => '100 Pills'),
							array('type' => 'product_cat', 'id' => 2237, 'name' => '50 Pills'),
							array('type' => 'product_cat', 'id' => 2238, 'name' => '30 Pills'),
							array('type' => 'product_cat', 'id' => 2239, 'name' => '20 Pills'),
							array('type' => 'product_cat', 'id' => 2240, 'name' => '10 Pills'),
						),
					),
					array(
						'title' => 'Form',
						'items' => array(
							array('type' => 'product_cat', 'id' => 107, 'name' => 'Viagra Soft Tabs (Jelly Gel/Liquid/Gum)'),
						),
					),
				),
				'product_cats_print_filter' => array(2224,367,366,44,138,139,107,2232,372,2233,2234,2235,2236,2237,2238,2239,2240),
				'parent_product_cat_id' => 2224 

			)
		);

		$this->filters_objects['cialis'] = new AGProductFilter(
			array(
				'filters_lists' => array(
					array(
						'title' => 'Dosage',
						'items' => array(
							array('type' => 'product_cat', 'id' => 368),
							array('type' => 'product_cat', 'id' => 369),
							array('type' => 'product_cat', 'id' => 370),
							array('type' => 'product_cat', 'id' => 51),
							array('type' => 'product_cat', 'id' => 134),
							array('type' => 'product_cat', 'id' => 135),
						),
					),
					array(
						'title' => 'Amount of Tablets',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2241, 'name' => '500 Pills'),
							array('type' => 'product_cat', 'id' => 2242, 'name' => '300 Pills'),
							array('type' => 'product_cat', 'id' => 2243, 'name' => '200 Pills'),
							array('type' => 'product_cat', 'id' => 2244, 'name' => '100 Pills'),
							array('type' => 'product_cat', 'id' => 2245, 'name' => '50 Pills'),
							array('type' => 'product_cat', 'id' => 2246, 'name' => '30 Pills'),
							array('type' => 'product_cat', 'id' => 2247, 'name' => '20 Pills'),
							array('type' => 'product_cat', 'id' => 2248, 'name' => '10 Pills'),
						),
					),
					array(
						'title' => 'Form',
						'items' => array(
							array('type' => 'product_cat', 'id' => 108, 'name' => 'Cialis Soft (Jelly Gel/Liquid/Gum)'),
						),
					),
				),
				'product_cats_print_filter' => array(2225,368,369,370,51,134,135,2241,2242,2243,2244,2245,2246,2247,2248,108),
				'parent_product_cat_id' => 2225

			)
		);

		$this->filters_objects['levitra'] = new AGProductFilter(
			array(
				'filters_lists' => array(
					array(
						'title' => 'Dosage',
						'items' => array(
							array('type' => 'product_cat', 'id' => 371, 'name' => 'Levitra 10 mg'),
							array('type' => 'product_cat', 'id' => 52,  'name' => 'Levitra 20 mg (Bestseller)'),
							array('type' => 'product_cat', 'id' => 136, 'name' => 'Levitra 40 mg'),
							array('type' => 'product_cat', 'id' => 137, 'name' => 'Levitra 60 mg'),
						),
					),
					array(
						'title' => 'Amount of Tablets',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2249, 'name' => '500 Pills'),
							array('type' => 'product_cat', 'id' => 2250, 'name' => '300 Pills'),
							array('type' => 'product_cat', 'id' => 2251, 'name' => '200 Pills'),
							array('type' => 'product_cat', 'id' => 2252, 'name' => '100 Pills'),
							array('type' => 'product_cat', 'id' => 2253, 'name' => '50 Pills'),
							array('type' => 'product_cat', 'id' => 2254, 'name' => '30 Pills'),
							array('type' => 'product_cat', 'id' => 2255, 'name' => '20 Pills'),
							array('type' => 'product_cat', 'id' => 2256, 'name' => '10 Pills'),
						),
					),
					array(
						'title' => 'Form',
						'items' => array(
							array('type' => 'product_cat', 'id' => 109, 'name' => 'Levitra Soft'),
						),
					),
				),
				'product_cats_print_filter' => array(2226,371,52,136,137,2249,2250,2251,2252,2253,2254,2255,2256,109),
				'parent_product_cat_id' => 2226

			)
		);

		$this->filters_objects['sildenafil'] = new AGProductFilter(
			array(
				'filters_lists' => array(
					array(
						'title' => 'Dosage',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2257,),
							array('type' => 'product_cat', 'id' => 2258,),
						),
					),
					array(
						'title' => 'Amount of Tablets',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2259, 'name' => '500 Pills'),
							array('type' => 'product_cat', 'id' => 2260, 'name' => '300 Pills'),
							array('type' => 'product_cat', 'id' => 2261, 'name' => '200 Pills'),
							array('type' => 'product_cat', 'id' => 2262, 'name' => '100 Pills'),
							array('type' => 'product_cat', 'id' => 2263, 'name' => '50 Pills'),
							array('type' => 'product_cat', 'id' => 2264, 'name' => '30 Pills'),
							array('type' => 'product_cat', 'id' => 2265, 'name' => '20 Pills'),
							array('type' => 'product_cat', 'id' => 2266, 'name' => '10 Pills'),
						),
					),
				),
				'product_cats_print_filter' => array(2227,2257,2258,2259,2260,2261,2262,2263,2264,2265,2266),
				'parent_product_cat_id' => 2227 

			)
		);

		$this->filters_objects['tadalafil'] = new AGProductFilter(
			array(
				'filters_lists' => array(
					array(
						'title' => 'Amount of Tablets',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2267, 'name' => '500 Pills'),
							array('type' => 'product_cat', 'id' => 2268, 'name' => '300 Pills'),
							array('type' => 'product_cat', 'id' => 2269, 'name' => '200 Pills'),
							array('type' => 'product_cat', 'id' => 2270, 'name' => '100 Pills'),
							array('type' => 'product_cat', 'id' => 2271, 'name' => '50 Pills'),
							array('type' => 'product_cat', 'id' => 2272, 'name' => '30 Pills'),
							array('type' => 'product_cat', 'id' => 2273, 'name' => '20 Pills'),
							array('type' => 'product_cat', 'id' => 2274, 'name' => '10 Pills'),
						),
					),
				),
				'product_cats_print_filter' => array(2228,2267,2268,2269,2270,2271,2272,2273,2274),
				'parent_product_cat_id' => 2228

			)
		);

		$this->filters_objects['main'] = new AGProductFilter(
			array(
				'filters_lists' => array(
					array(
						'title' => 'Best Sellers',
						'items' => array(
							array('type' => 'product_cat', 'id' => 45, 'name' => 'Free Pills for ED'),
							array('type' => 'product_cat', 'id' => 44, 'name' => 'Viagra 100 mg'),
							array('type' => 'product_cat', 'id' => 51, 'name' => 'Cialis 20 mg'),
							array('type' => 'product_cat', 'id' => 52, 'name' => 'Levitra 20 mg'),
							array('type' => 'product_cat', 'id' => 743),
						), 
					),
					array(
						'title' => 'Sexual Weakness',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2222),
							array('type' => 'product_cat', 'id' => 2223),
						),
					),
					array(
						'title' => 'Brand',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2224),
							array('type' => 'product_cat', 'id' => 2225),
							array('type' => 'product_cat', 'id' => 2226),
							array('type' => 'product_cat', 'id' => 410, 'name' => 'Stendra'),
							array('type' => 'product_cat', 'id' => 103, 'name' => 'Priligy'),
							array('type' => 'product_cat', 'id' => 286),
							array('type' => 'product_cat', 'id' => 285),
							array('type' => 'product_cat', 'id' => 382, 'name' => 'Kamagra'),
							array('type' => 'product_cat', 'id' => 287, 'name' => 'Super Zhewitra'),
						),
					),
					array(
						'title' => 'Active Substance',
						'items' => array(
							array('type' => 'product_cat', 'id' => 2227),
							array('type' => 'product_cat', 'id' => 2228),
							array('type' => 'product_cat', 'id' => 2229),
							array('type' => 'product_cat', 'id' => 340, 'name' => 'Modafinil'),
							array('type' => 'product_cat', 'id' => 2230),
							array('type' => 'product_cat', 'id' => 2231),
						),
					),
					array(
						'title' => 'Alternatives',
						'items' => array(
							array('type' => 'product_cat', 'id' => 45, 'name' => 'Free Pills for ED'),
							array('type' => 'product_cat', 'id' => 44, 'name' => 'Viagra 100 mg'),
							array('type' => 'product_cat', 'id' => 51, 'name' => 'Cialis 20 mg'),
							array('type' => 'product_cat', 'id' => 52, 'name' => 'Levitra 20 mg'),
						), 
					),
				)
			)
		);


		$this->filters = array(
			'main' => array(
				array(
					'title' => 'Best Sellers',
					'items' => array(
						array('type' => 'product_cat', 'id' => 45, 'name' => 'Free Pills for ED'),
						array('type' => 'product_cat', 'id' => 44, 'name' => 'Viagra 100 mg'),
						array('type' => 'product_cat', 'id' => 51, 'name' => 'Cialis 20 mg'),
						array('type' => 'product_cat', 'id' => 52, 'name' => 'Levitra 20 mg'),
						array('type' => 'product_cat', 'id' => 743),
					), 
				),
				array(
					'title' => 'Sexual Weakness',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2222),
						array('type' => 'product_cat', 'id' => 2223),
					),
				),
				array(
					'title' => 'Brand',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2224),
						array('type' => 'product_cat', 'id' => 2225),
						array('type' => 'product_cat', 'id' => 2226),
						array('type' => 'product_cat', 'id' => 410, 'name' => 'Stendra'),
						array('type' => 'product_cat', 'id' => 103, 'name' => 'Priligy'),
						array('type' => 'product_cat', 'id' => 286),
						array('type' => 'product_cat', 'id' => 285),
						array('type' => 'product_cat', 'id' => 382, 'name' => 'Kamagra'),
						array('type' => 'product_cat', 'id' => 287, 'name' => 'Super Zhewitra'),
					),
				),
				array(
					'title' => 'Active Substance',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2227),
						array('type' => 'product_cat', 'id' => 2228),
						array('type' => 'product_cat', 'id' => 2229),
						array('type' => 'product_cat', 'id' => 340, 'name' => 'Modafinil'),
						array('type' => 'product_cat', 'id' => 2230),
						array('type' => 'product_cat', 'id' => 2231),
					),
				),
				array(
					'title' => 'Alternatives',
					'items' => array(
						array('type' => 'product_cat', 'id' => 45, 'name' => 'Free Pills for ED'),
						array('type' => 'product_cat', 'id' => 44, 'name' => 'Viagra 100 mg'),
						array('type' => 'product_cat', 'id' => 51, 'name' => 'Cialis 20 mg'),
						array('type' => 'product_cat', 'id' => 52, 'name' => 'Levitra 20 mg'),
					), 
				),
			),
			'viagra' => array(
				array(
					'title' => 'Dosage',
					'items' => array(
						array('type' => 'product_cat', 'id' => 367),
						array('type' => 'product_cat', 'id' => 366),
						array('type' => 'product_cat', 'id' => 44),
						array('type' => 'product_cat', 'id' => 138),
						array('type' => 'product_cat', 'id' => 139),
					),
				),
				array(
					'title' => 'Sex',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2232),
						array('type' => 'product_cat', 'id' => 372),
					),
				),
				array(
					'title' => 'Amount of Tablets',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2233, 'name' => '500 Pills'),
						array('type' => 'product_cat', 'id' => 2234, 'name' => '300 Pills'),
						array('type' => 'product_cat', 'id' => 2235, 'name' => '200 Pills'),
						array('type' => 'product_cat', 'id' => 2236, 'name' => '100 Pills'),
						array('type' => 'product_cat', 'id' => 2237, 'name' => '50 Pills'),
						array('type' => 'product_cat', 'id' => 2238, 'name' => '30 Pills'),
						array('type' => 'product_cat', 'id' => 2239, 'name' => '20 Pills'),
						array('type' => 'product_cat', 'id' => 2240, 'name' => '10 Pills'),
					),
				),
				array(
					'title' => 'Form',
					'items' => array(
						array('type' => 'product_cat', 'id' => 107, 'name' => 'Viagra Soft Tabs (Jelly Gel/Liquid/Gum)'),
					),
				),
			),
			'cialis' => array(
				array(
					'title' => 'Dosage',
					'items' => array(
						array('type' => 'product_cat', 'id' => 368),
						array('type' => 'product_cat', 'id' => 369),
						array('type' => 'product_cat', 'id' => 370),
						array('type' => 'product_cat', 'id' => 51),
						array('type' => 'product_cat', 'id' => 134),
						array('type' => 'product_cat', 'id' => 135),
					),
				),
				array(
					'title' => 'Amount of Tablets',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2241, 'name' => '500 Pills'),
						array('type' => 'product_cat', 'id' => 2242, 'name' => '300 Pills'),
						array('type' => 'product_cat', 'id' => 2243, 'name' => '200 Pills'),
						array('type' => 'product_cat', 'id' => 2244, 'name' => '100 Pills'),
						array('type' => 'product_cat', 'id' => 2245, 'name' => '50 Pills'),
						array('type' => 'product_cat', 'id' => 2246, 'name' => '30 Pills'),
						array('type' => 'product_cat', 'id' => 2247, 'name' => '20 Pills'),
						array('type' => 'product_cat', 'id' => 2248, 'name' => '10 Pills'),
					),
				),
				array(
					'title' => 'Form',
					'items' => array(
						array('type' => 'product_cat', 'id' => 108, 'name' => 'Cialis Soft (Jelly Gel/Liquid/Gum)'),
					),
				),
			),
			'levitra' => array(
				array(
					'title' => 'Dosage',
					'items' => array(
						array('type' => 'product_cat', 'id' => 371, 'name' => 'Levitra 10 mg'),
						array('type' => 'product_cat', 'id' => 52,  'name' => 'Levitra 20 mg (Bestseller)'),
						array('type' => 'product_cat', 'id' => 136, 'name' => 'Levitra 40 mg'),
						array('type' => 'product_cat', 'id' => 137, 'name' => 'Levitra 60 mg'),
					),
				),
				array(
					'title' => 'Amount of Tablets',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2249, 'name' => '500 Pills'),
						array('type' => 'product_cat', 'id' => 2250, 'name' => '300 Pills'),
						array('type' => 'product_cat', 'id' => 2251, 'name' => '200 Pills'),
						array('type' => 'product_cat', 'id' => 2252, 'name' => '100 Pills'),
						array('type' => 'product_cat', 'id' => 2253, 'name' => '50 Pills'),
						array('type' => 'product_cat', 'id' => 2254, 'name' => '30 Pills'),
						array('type' => 'product_cat', 'id' => 2255, 'name' => '20 Pills'),
						array('type' => 'product_cat', 'id' => 2256, 'name' => '10 Pills'),
					),
				),
				array(
					'title' => 'Form',
					'items' => array(
						array('type' => 'product_cat', 'id' => 109, 'name' => 'Levitra Soft'),
					),
				),
			),
			'sildenafil' => array(
				array(
					'title' => 'Dosage',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2257,),
						array('type' => 'product_cat', 'id' => 2258,),
					),
				),
				array(
					'title' => 'Amount of Tablets',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2259, 'name' => '500 Pills'),
						array('type' => 'product_cat', 'id' => 2260, 'name' => '300 Pills'),
						array('type' => 'product_cat', 'id' => 2261, 'name' => '200 Pills'),
						array('type' => 'product_cat', 'id' => 2262, 'name' => '100 Pills'),
						array('type' => 'product_cat', 'id' => 2263, 'name' => '50 Pills'),
						array('type' => 'product_cat', 'id' => 2264, 'name' => '30 Pills'),
						array('type' => 'product_cat', 'id' => 2265, 'name' => '20 Pills'),
						array('type' => 'product_cat', 'id' => 2266, 'name' => '10 Pills'),
					),
				),
			),
			'tadalafil' => array(
				array(
					'title' => 'Amount of Tablets',
					'items' => array(
						array('type' => 'product_cat', 'id' => 2267, 'name' => '500 Pills'),
						array('type' => 'product_cat', 'id' => 2268, 'name' => '300 Pills'),
						array('type' => 'product_cat', 'id' => 2269, 'name' => '200 Pills'),
						array('type' => 'product_cat', 'id' => 2270, 'name' => '100 Pills'),
						array('type' => 'product_cat', 'id' => 2271, 'name' => '50 Pills'),
						array('type' => 'product_cat', 'id' => 2272, 'name' => '30 Pills'),
						array('type' => 'product_cat', 'id' => 2273, 'name' => '20 Pills'),
						array('type' => 'product_cat', 'id' => 2274, 'name' => '10 Pills'),
					),
				),
			),
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		$this->print_filters();
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Product Filters', 'ag_text_domain' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'ag_text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}

	public function print_filters()
	{
		// echo '<pre>$this->filters = '.htmlspecialchars(print_r($this->filters, true)).'</pre>';
		// echo $this->get_filters_html();
		$filters = $this->get_current_filters();
		if (is_array($filters) && is_user_logged_in()) {
			// global $wp_query;
			// echo '<pre style="display:none;">$wp_query = '.htmlspecialchars(print_r($wp_query, true)).'</pre>';
			foreach ($filters as $filter_lists) : ?>
				<h5><p class="ag-filter-title"><span><?php echo $filter_lists['title'] ?></span></p></h5>
				<ul class="ag-product-filters">
					<?php 
					$format_link = '<a href="%2$s">%1$s</a>';
					$format_span = '<span>%s</span>';
					foreach ($filter_lists['items'] as $item) : 
						$format_output = $format_link;
						if($item['name'] && $item['link']){
							$name = $item['name'];
							$link = $item['link'];
						} elseif($item['type'] == 'product_cat'){
							$cat = get_term( $item['id'], 'product_cat' );
							if(!$cat->count)
								continue;
							$name = $item['name'] ? $item['name'] : $cat->name;
							$link = $item['link'] ? $item['link'] : get_term_link($cat, 'product_cat');
							if(is_tax( 'product_cat',$item['id']))
								$format_output = $format_span;
						} /*elseif($item['type'] == 'post'){
							$post_item = get_post( $item['id'] );
							$name = $item['name'] ? $item['name'] : $post_item->post_title;
							$link = $item['link'] ? $item['link'] : get_permalink( $post_item );
						}*/

						printf('<li class="ag-filter-item">'.$format_output.'</li>', $name, $link);
						
					endforeach  ?>
				</ul>
			<?php endforeach;
		} ?>
		<style>
			.widget_agproductfilter_widget{display: none;}
		</style>
		<?php
	}

	/*public function get_filters_html()
	{
		return $filters;
	}*/

	public function get_current_filters()
	{
		$filters2 = array();
		
		foreach ($this->filters_objects as $value) {
			if(!$value->product_cats_print_filter || is_tax( 'product_cat', $value->product_cats_print_filter) )
				$filters2 = array_merge($filters2, $value->filters_lists);
		}

		// echo '<pre style="display:none;">$filters2 = '.htmlspecialchars(print_r($filters2, true)).'</pre>';

		// $filters = $this->filters['main'];
		/*if( is_front_page() || is_singular('post') || (function_exists('is_product_category') && is_product_category()) ){
			$filters = $this->filters['main'];
		} else*/
		/*if(is_page( 149203 ))
			$filters = array_merge($this->filters['viagra'], $filters);
		elseif(is_page( 149205 ))
			$filters = array_merge($this->filters['cialis'], $filters);
		elseif(is_page( 149207 ))
			$filters = array_merge($this->filters['levitra'], $filters);
		elseif(is_page( 149210 ))
			$filters = array_merge($this->filters['sildenafil'], $filters);
		elseif(is_page( 149212 ))
			$filters = array_merge($this->filters['tadalafil'], $filters);*/
		/*if(is_tax( 'product_cat', array(2224,367,366,44,138,139,107,2232,372,2233,2234,2235,2236,2237,2238,2239,2240) ))
			$filters = array_merge($this->filters['viagra'], $filters);
		elseif(is_tax( 'product_cat', array(2225,368,369,370,51,134,135,2241,2242,2243,2244,2245,2246,2247,2248,108) ))
			$filters = array_merge($this->filters['cialis'], $filters);
		elseif(is_tax( 'product_cat', array(2226,371,52,136,137,2249,2250,2251,2252,2253,2254,2255,2256,109) ))
			$filters = array_merge($this->filters['levitra'], $filters);
		elseif(is_tax( 'product_cat', array(2227,2257,2258,2259,2260,2261,2262,2263,2264,2265,2266) ))
			$filters = array_merge($this->filters['sildenafil'], $filters);
		elseif(is_tax( 'product_cat', array(2228,2267,2268,2269,2270,2271,2272,2273,2274) ))
			$filters = array_merge($this->filters['tadalafil'], $filters);
		else
			$filters = $this->filters['main'];*/

		// echo '<pre style="display:none;">$filters = '.htmlspecialchars(print_r($filters, true)).'</pre>';
		return $filters2;
	}

} // class AGProductFilter_widget

// register AGProductFilter_widget widget
function register_AGProductFilter_widget() {
    register_widget( 'AGProductFilter_widget' );
}
add_action( 'widgets_init', 'register_AGProductFilter_widget' );


?>